
#ifndef DL_LDH_PACKET_SCHEDULER_H_
#define DL_LDH_PACKET_SCHEDULER_H_

#include "downlink-packet-scheduler.h"

class DL_LDH_PacketScheduler : public DownlinkPacketScheduler {
	public:
		DL_LDH_PacketScheduler();
		virtual ~DL_LDH_PacketScheduler();
		
		virtual void DoSchedule(void);
		virtual double ComputeSchedulingMetric(RadioBearer* bearer, double spectralEfficiency, int subChannel);
	
};

#endif 