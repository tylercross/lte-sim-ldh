
#ifndef DL_PAPER_PACKET_SCHEDULER_H_
#define DL_PAPER_PACKET_SCHEDULER_H_

#include "downlink-packet-scheduler.h"

class DL_PAPER_PacketScheduler : public DownlinkPacketScheduler{
public:
	DL_PAPER_PacketScheduler();
	virtual ~DL_PAPER_PacketScheduler();

	virtual void DoSchedule(void);
	virtual double ComputeSchedulingMetric(RadioBearer* bearer, double spectralEfficiency, int subChannel);

};

#endif
