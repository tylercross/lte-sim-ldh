#ifndef DL_LDF_PACKET_SCHEDULER_H_
#define DL_LDF_PACKET_SCHEDULER_H_

#include "downlink-packet-scheduler.h"

class DL_LDF_PacketScheduler : public DownlinkPacketScheduler {
public:
	DL_LDF_PacketScheduler();
	virtual ~DL_LDF_PacketScheduler();

	virtual void DoSchedule (void);

	virtual double ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel);
};

#endif /* DL_MLWDF_PACKET_SCHEDULER_H_ */