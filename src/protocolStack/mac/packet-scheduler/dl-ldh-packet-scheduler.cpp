#include "dl-ldh-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../packet/PacketTAGs.h"    // different from downlink-packet-scheduler
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForM_LWDF.h"
//add QosForLDH
#include "../../../flows/QoS/QoSForLDH.h"
#include "../../../flows/MacQueue.h"
#include "../../../utility/eesm-effective-sinr.h"

DL_LDH_PacketScheduler::DL_LDH_PacketScheduler()
{
	SetMacEntity(0);
	CreateFlowsToSchedule();
}

DL_LDH_PacketScheduler::~DL_LDH_PacketScheduler()
{
	Destroy();
}

void DL_LDH_PacketScheduler::DoSchedule()
{
	#ifdef SCHEDULER_DEBUG
		std::cout << "start DL packet scheduler for node "
				  << GetMacEntity() -> GetDevice() -> GetIDNetworkNode()
				  << std::endl;
	#endif

	UpdateAverageTransmissionRate();
	CheckForDLDropPackets();
	SelectFlowsToSchedule();

	if(GetFlowsToSchedule() -> size() == 0)
	{

	}
	else
	{
		RBsAllocation();
	}

	StopSchedule();
}

double DL_LDH_PacketScheduler::ComputeSchedulingMetric(RadioBearer* bearer, double spectralEfficiency, int subChannel)
{
	/*
	 for the LDH scheduler the metric is computed as follows:
	 metric = *availableRate/averageRates + *HOL/targetDelay
	                                       + *importance_index
	 for simplicity, we set  = =  = 1.
	*/
	
	double metric;

	// INFINITE_BUFFER or CBR application
	if((bearer -> GetApplication() -> GetApplicationType() == Application::APPLICATION_TYPE_INFINITE_BUFFER)
		||
	   (bearer -> GetApplication() -> GetApplicationType() == Application::APPLICATION_TYPE_CBR))  
	{
		metric = (spectralEfficiency * 180000.)
		          / 
		          bearer -> GetAverageTransmissionRate();

		#ifdef SCHEDULER_DEBUG
			std::cout << "METRIC: " << bearer -> GetApplication() -> GetApplicationID()
					  << " " << spectralEfficiency
					  << " " << bearer -> GetAverageTransmissionRate()
					  << " --> " << metric
					  << std::endl;
		#endif
	} 

	// real time application(default video)
	else
	{
		QoSForLDH* qos = (QoSForLDH*) bearer -> GetQoSParameters();
		
		double HOL = bearer -> GetHeadOfLinePacketDelay();
		double importance_index = 1.0;  // depends on the structure of GOP

		//  downlink-packet-scheduler.cpp/HasIFrame

		FlowToSchedule* flow; //add
		RadioBearer* bear = flow -> m_bearer;
		MacQueue* mac_queue = bear -> GetMacQueue();
		PacketTAGs* pkt_tags = flow -> m_bearer -> GetMacQueue() -> Peek().GetPacket() -> GetPacketTags();
		if(pkt_tags -> GetApplicationType() == PacketTAGs::APPLICATION_TYPE_TRACE_BASED)
		{
			if(pkt_tags -> GetFrameType() == 'I')
				importance_index = 11.0;
			else
				if(pkt_tags -> GetFrameType() == 'P')
					importance_index = 9.0;
		}	


		metric = (spectralEfficiency*180000.)/bearer->GetAverageTransmissionRate()
		       + HOL/qos->GetMaxDelay() + importance_index;

		#ifdef SCHEDULER_DEBUG
		       std::cout << "METRIC: " << bearer -> GetApplication() ->GetApplicationID()
		                 << " " << a
		                 << " " << Simulator::Init() -> Now()
		                 << " " << bearer -> GetMacQueue() -> Peek().GetTimeStamp()
		                 << " " << HOL
		                 << " " << spectralEfficiency
		                 << " " << bearer -> GetAverageTransmissionRate()
		                 << " --> " << metric
		                 << std::endl;
		#endif	                
	}   
	return metric;                           
}
