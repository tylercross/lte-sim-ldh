/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include "dl-ipb-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForIPB.h"
#include "../../../flows/MacQueue.h"
#include "../../../utility/eesm-effective-sinr.h"

DL_IPB_PacketScheduler::DL_IPB_PacketScheduler()
{
  SetMacEntity (0);
  CreateFlowsToSchedule ();
}

DL_IPB_PacketScheduler::~DL_IPB_PacketScheduler()
{
  Destroy ();
}

//考虑两种分配资源块的方案：
//1、当频谱利用效率大于 1 时才选择优先给I帧的流分配资源
//2、按2:8比例来给非I帧、I帧流分配资源
//先考虑实现第一种方案
void
DL_IPB_PacketScheduler::RBsAllocation ()
{
#ifdef SCHEDULER_DEBUG
  std::cout << " ---- DL_IPB_PacketScheduler::RBsAllocation" << std::endl;
#endif


  FlowsToSchedule* flows = GetFlowsToSchedule ();
  int nbOfRBs = GetMacEntity ()->GetDevice ()->GetPhy ()->GetBandwidthManager ()->GetDlSubChannels ().size ();

  //int ant_iRBsForFlowWithI = 0.8 * nbOfRBs;  //分配给I_Frame流的资源块数目  max=80
  //int ant_iRBsForFlowWithoutI = nbOfRBs - ant_iRBsForFlowWithI;  //分配给不含I_Frame流的资源块数目 max=20

  //create a matrix of flow metrics
  double metrics[nbOfRBs][flows->size ()];
  for (int i = 0; i < nbOfRBs; i++)
    {
    for (int j = 0; j < flows->size (); j++)
      {
      metrics[i][j] = ComputeSchedulingMetric (flows->at (j)->GetBearer (),
                                           flows->at (j)->GetSpectralEfficiency ().at (i),
                                             i);
      }
    }

#ifdef SCHEDULER_DEBUG
  std::cout << ", available RBs " << nbOfRBs << ", flows " << flows->size () << std::endl;
  for (int ii = 0; ii < flows->size (); ii++)
    {
    std::cout << "\t metrics for flow "
        << flows->at (ii)->GetBearer ()->GetApplication ()->GetApplicationID () << ":";
    for (int jj = 0; jj < nbOfRBs; jj++)
      {
      std::cout << " " << metrics[jj][ii];
      }
    std::cout << std::endl;
    }
#endif


  AMCModule *amc = GetMacEntity ()->GetAmcModule ();
  double l_dAllocatedRBCounter = 0;  //已分配RB数量

  //int l_iNumberOfUsers = ((ENodeB*)this->GetMacEntity()->GetDevice())->GetNbOfUserEquipmentRecords(); //用户数量

  bool * l_bFlowScheduled = new bool[flows->size ()]; //flow是否已调度
  int l_iScheduledFlows = 0;  //已调度的流的数量
  std::vector<double> * l_bFlowScheduledSINR = new std::vector<double>[flows->size ()];  //每个流的SINR
  for (int k = 0; k < flows->size (); k++)
      l_bFlowScheduled[k] = false; //初始所有流的调度状态为false--未调度。
  
  bool * ant_bFlowWithI = new bool[flows->size()];  //true表示这个流包含I帧的包
  int ant_iFlowWithICount = 0; //I_Frame流的数量
  int ant_iFlowWithI = 0;  //记录已调度的I_Frame流的数量
  for(int i = 0; i < flows->size(); ++i){
    if(HasIFrame(flows->at(i))){
      ant_bFlowWithI[i] = true;
      ant_iFlowWithICount++;
    }
    else {
      ant_bFlowWithI[i] = false;
    }
  }

  //RBs allocation
  for (int s = 0; s < nbOfRBs; s++)
  {
      if (l_iScheduledFlows == flows->size ())    //若所有待调度流都已调度，则结束剩余资源块的分配。
          break;

      //double targetMetric = 0;
      bool RBIsAllocated = false;
      FlowToSchedule* scheduledFlow;  //当前RB应该分配给此flow
      int l_iScheduledFlowIndex = 0;

      double targetMetric4Iflow = 0;
      FlowToSchedule* scheduledFlow4Iflow;  //当前RB上含I帧的流的最大优先级
      int ant_iScheduledFlowIndex4Iflow = 0;

      double targetMetric4noIflow = 0;
      FlowToSchedule* scheduledFlow4noIflow;  //当前RB上 不 含I帧的流的最大优先级
      int ant_iScheduledFlowIndex4noIflow = 0;

      for (int k = 0; k < flows->size (); k++)
      {
          //分别记录含I帧的流和不含I帧的流的最大优先级及对应的流
          if(metrics[s][k] > targetMetric4noIflow && !l_bFlowScheduled[k] && !ant_bFlowWithI[k]){
            targetMetric4noIflow = metrics[s][k];
            RBIsAllocated = true;
            scheduledFlow4noIflow = flows->at (k);
            ant_iScheduledFlowIndex4noIflow = k;
          }

          else if(metrics[s][k] > targetMetric4Iflow && !l_bFlowScheduled[k] && ant_bFlowWithI[k]){
            targetMetric4Iflow = metrics[s][k];
            RBIsAllocated = true;
            scheduledFlow4Iflow = flows->at(k);
            ant_iScheduledFlowIndex4Iflow = k;
          }        
      }
      if(targetMetric4Iflow >= targetMetric4noIflow){
        scheduledFlow = scheduledFlow4Iflow;
        l_iScheduledFlowIndex = ant_iScheduledFlowIndex4Iflow;
      }
      //在此需要在此判断Index4Iflow流是否为含有I帧的流
      //因为可能没有含I帧的流，那么Index4Iflow为0，scheduledFlow4Iflow 没有实际值，会导致程序异常中断。
      else if(ant_bFlowWithI[ant_iScheduledFlowIndex4Iflow] && flows->at (ant_iScheduledFlowIndex4Iflow)->GetSpectralEfficiency ().at (s) > 1.7){
        scheduledFlow = scheduledFlow4Iflow;
        l_iScheduledFlowIndex = ant_iScheduledFlowIndex4Iflow;
      }
      else{
        scheduledFlow = scheduledFlow4noIflow;
        l_iScheduledFlowIndex = ant_iScheduledFlowIndex4noIflow;
      }

      if (RBIsAllocated) //如果有资源块被分配
        {
          l_dAllocatedRBCounter++;  //已分配资源块计数加一

          scheduledFlow->GetListOfAllocatedRBs()->push_back (s); // the s RB has been allocated to that flow!

#ifdef SCHEDULER_DEBUG
          std::cout << "\t *** RB " << s << " assigned to the "
                  " flow " << scheduledFlow->GetBearer ()->GetApplication ()->GetApplicationID ()
                  << std::endl;
#endif
          double sinr = amc->GetSinrFromCQI (scheduledFlow->GetCqiFeedbacks ().at (s));
          l_bFlowScheduledSINR[l_iScheduledFlowIndex].push_back(sinr);

          double effectiveSinr = GetEesmEffectiveSinr (l_bFlowScheduledSINR[l_iScheduledFlowIndex]);
          int mcs = amc->GetMCSFromCQI (amc->GetCQIFromSinr (effectiveSinr));
          int transportBlockSize = amc->GetTBSizeFromMCS (mcs, scheduledFlow->GetListOfAllocatedRBs ()->size ());
          if (transportBlockSize >= scheduledFlow->GetDataToTransmit() * 8)  //传输块大小 大于 要传输的数据量，则已能全部传输，无需再分配资源
          {
              l_bFlowScheduled[l_iScheduledFlowIndex] = true;
              l_iScheduledFlows++;
              if(ant_bFlowWithI[l_iScheduledFlowIndex] == true){    //如果此流是I_Frame流，那么以满足调度需求的I_Frame计数加一
                ant_iFlowWithI++;
              }
          }

        }
    }//所有资源块分配结束 或 所有流分配的资源都已满足传输需求

  delete [] l_bFlowScheduled;
  delete [] l_bFlowScheduledSINR;


  //Finalize the allocation 
  //资源块分配好后，对每一个流进行传输
  PdcchMapIdealControlMessage *pdcchMsg = new PdcchMapIdealControlMessage ();

  for (FlowsToSchedule::iterator it = flows->begin (); it != flows->end (); it++)
    {
      FlowToSchedule *flow = (*it);
      if (flow->GetListOfAllocatedRBs ()->size () > 0)
        {
          //this flow has been scheduled
          std::vector<double> estimatedSinrValues;
          for (int rb = 0; rb < flow->GetListOfAllocatedRBs ()->size (); rb++ )

            {
              double sinr = amc->GetSinrFromCQI (
                      flow->GetCqiFeedbacks ().at (flow->GetListOfAllocatedRBs ()->at (rb)));

              estimatedSinrValues.push_back (sinr);
            }

          //compute the effective sinr
          double effectiveSinr = GetEesmEffectiveSinr (estimatedSinrValues);

          //get the MCS for transmission

          int mcs = amc->GetMCSFromCQI (amc->GetCQIFromSinr (effectiveSinr));

          //define the amount of bytes to transmit
          //int transportBlockSize = amc->GetTBSizeFromMCS (mcs);
          int transportBlockSize = amc->GetTBSizeFromMCS (mcs, flow->GetListOfAllocatedRBs ()->size ());
          double bitsToTransmit = transportBlockSize;
          flow->UpdateAllocatedBits (bitsToTransmit);

#ifdef SCHEDULER_DEBUG
      std::cout << "\t\t --> flow " << flow->GetBearer ()->GetApplication ()->GetApplicationID ()
          << " has been scheduled: " <<
          "\n\t\t\t nb of RBs " << flow->GetListOfAllocatedRBs ()->size () <<
          "\n\t\t\t effectiveSinr " << effectiveSinr <<
          "\n\t\t\t tbs " << transportBlockSize <<
          "\n\t\t\t bitsToTransmit " << bitsToTransmit
          << std::endl;
#endif

      //create PDCCH messages
      for (int rb = 0; rb < flow->GetListOfAllocatedRBs ()->size (); rb++ )
        {
        pdcchMsg->AddNewRecord (PdcchMapIdealControlMessage::DOWNLINK,
            flow->GetListOfAllocatedRBs ()->at (rb),
                    flow->GetBearer ()->GetDestination (),
                    mcs);
        }
      }
    }

  if (pdcchMsg->GetMessage()->size () > 0)
    {
      GetMacEntity ()->GetDevice ()->GetPhy ()->SendIdealControlMessage (pdcchMsg);
    }
  delete pdcchMsg;
}

/*
// 此方法无脑得给含I帧的流分配资源块
void
DL_IPB_PacketScheduler::RBsAllocation ()
{
#ifdef SCHEDULER_DEBUG
	std::cout << " ---- DL_IPB_PacketScheduler::RBsAllocation";
#endif


  FlowsToSchedule* flows = GetFlowsToSchedule ();
  int nbOfRBs = GetMacEntity ()->GetDevice ()->GetPhy ()->GetBandwidthManager ()->GetDlSubChannels ().size ();

  //create a matrix of flow metrics
  double metrics[nbOfRBs][flows->size ()];
  for (int i = 0; i < nbOfRBs; i++)
    {
	  for (int j = 0; j < flows->size (); j++)
	    {
		  metrics[i][j] = ComputeSchedulingMetric (flows->at (j)->GetBearer (),
				                                   flows->at (j)->GetSpectralEfficiency ().at (i),
	    		                                   i);
	    }
    }

#ifdef SCHEDULER_DEBUG
  std::cout << ", available RBs " << nbOfRBs << ", flows " << flows->size () << std::endl;
  for (int ii = 0; ii < flows->size (); ii++)
    {
	  std::cout << "\t metrics for flow "
			  << flows->at (ii)->GetBearer ()->GetApplication ()->GetApplicationID () << ":";
	  for (int jj = 0; jj < nbOfRBs; jj++)
	    {
		  std::cout << " " << metrics[jj][ii];
	    }
	  std::cout << std::endl;
    }
#endif


AMCModule *amc = GetMacEntity ()->GetAmcModule ();
  double l_dAllocatedRBCounter = 0;  //已分配RB数量

  int l_iNumberOfUsers = ((ENodeB*)this->GetMacEntity()->GetDevice())->GetNbOfUserEquipmentRecords(); //用户数量

  bool * l_bFlowScheduled = new bool[flows->size ()]; //flow是否已调度
  int l_iScheduledFlows = 0;  //已调度的流
  std::vector<double> * l_bFlowScheduledSINR = new std::vector<double>[flows->size ()];  //每个流的SINR
  for (int k = 0; k < flows->size (); k++)
      l_bFlowScheduled[k] = false; //初始所有流的调度状态为false--未调度。
  
  bool * ant_bFlowWithI = new bool[flows->size()];  //true表示这个流包含I帧的包
  int ant_iFlowWithICount = 0; //I_Frame流的数量
  int ant_iFlowWithI = 0;  //记录已调度的I_Frame流的数量
  for(int i = 0; i < flows->size(); ++i){
    //if(flows->at(i)->HasIFrame()){         // HasIFrame()函数待完成
  	if(HasIFrame(flows->at(i))){
		//if(flows->at(i)->m_bearer->GetMacQueue()->Peek().GetPacket()->GetPacketTags()->GetApplicationType()==PacketTAGs::APPLICATION_TYPE_TRACE_BASED){
		//	if(flows->at(i)->m_bearer->GetMacQueue()->Peek().GetPacket()->GetPacketTags()->GetFrameType() == 'I'){
	    ant_bFlowWithI[i] = true;
	    ant_iFlowWithICount++;
	  }
	  else {
	    ant_bFlowWithI[i] = false;
	  }
  }

  //RBs allocation
  for (int s = 0; s < nbOfRBs; s++)
    {
      if (l_iScheduledFlows == flows->size ())    //若所有待调度流都已调度，则结束剩余资源块的分配。
          break;

      double targetMetric = 0;
      bool RBIsAllocated = false;
      FlowToSchedule* scheduledFlow;
      int l_iScheduledFlowIndex = 0;
      
      if(ant_iFlowWithI < ant_iFlowWithICount) {    //还有所分配资源未满足传输需求的I_Frame流
        for (int k = 0; k < flows->size (); k++)
        {
          if (ant_bFlowWithI[k] && !l_bFlowScheduled[k] && metrics[s][k] > targetMetric) //在还未调度的流中，选出含有I帧的流中metrics最大的一个。
            {
              targetMetric = metrics[s][k];
              RBIsAllocated = true;
              scheduledFlow = flows->at (k);
              l_iScheduledFlowIndex = k;
            }
        }
      }
      else { //所有I_Frame流已分配足够传输资源，剩余资源块分配给其他流。
        for (int k = 0; k < flows->size (); k++)
        {
          if (metrics[s][k] > targetMetric && !l_bFlowScheduled[k] && !ant_bFlowWithI[k]) //在还未调度的流中，选出流中metrics最大的一个。
            {
              targetMetric = metrics[s][k];
              RBIsAllocated = true;
              scheduledFlow = flows->at (k);
              l_iScheduledFlowIndex = k;
            }
        }
      }

      if (RBIsAllocated) //如果有资源块被分配
        {
          l_dAllocatedRBCounter++;  //已分配资源块计数加一

          scheduledFlow->GetListOfAllocatedRBs()->push_back (s); // the s RB has been allocated to that flow!

#ifdef SCHEDULER_DEBUG
          std::cout << "\t *** RB " << s << " assigned to the "
                  " flow " << scheduledFlow->GetBearer ()->GetApplication ()->GetApplicationID ()
                  << std::endl;
#endif
          double sinr = amc->GetSinrFromCQI (scheduledFlow->GetCqiFeedbacks ().at (s));
          l_bFlowScheduledSINR[l_iScheduledFlowIndex].push_back(sinr);

          double effectiveSinr = GetEesmEffectiveSinr (l_bFlowScheduledSINR[l_iScheduledFlowIndex]);
          int mcs = amc->GetMCSFromCQI (amc->GetCQIFromSinr (effectiveSinr));
          int transportBlockSize = amc->GetTBSizeFromMCS (mcs, scheduledFlow->GetListOfAllocatedRBs ()->size ());
          if (transportBlockSize >= scheduledFlow->GetDataToTransmit() * 8)  //传输块大小 大于 要传输的数据量，则已能全部传输，无需再分配资源
          {
              l_bFlowScheduled[l_iScheduledFlowIndex] = true;
              l_iScheduledFlows++;
              if(ant_bFlowWithI[l_iScheduledFlowIndex] == true){    //如果此流是I_Frame流，那么以满足调度需求的I_Frame计数加一
              	ant_iFlowWithI++;
              }
          }

        }
    }//所有资源块分配结束 或 所有流分配的资源都已满足传输需求

  delete [] l_bFlowScheduled;
  delete [] l_bFlowScheduledSINR;


  //Finalize the allocation 
  //资源块分配好后，对每一个流进行传输
  PdcchMapIdealControlMessage *pdcchMsg = new PdcchMapIdealControlMessage ();

  for (FlowsToSchedule::iterator it = flows->begin (); it != flows->end (); it++)
    {
      FlowToSchedule *flow = (*it);
      if (flow->GetListOfAllocatedRBs ()->size () > 0)
        {
          //this flow has been scheduled
          std::vector<double> estimatedSinrValues;
          for (int rb = 0; rb < flow->GetListOfAllocatedRBs ()->size (); rb++ )

            {
              double sinr = amc->GetSinrFromCQI (
                      flow->GetCqiFeedbacks ().at (flow->GetListOfAllocatedRBs ()->at (rb)));

              estimatedSinrValues.push_back (sinr);
            }

          //compute the effective sinr
          double effectiveSinr = GetEesmEffectiveSinr (estimatedSinrValues);

          //get the MCS for transmission

          int mcs = amc->GetMCSFromCQI (amc->GetCQIFromSinr (effectiveSinr));

          //define the amount of bytes to transmit
          //int transportBlockSize = amc->GetTBSizeFromMCS (mcs);
          int transportBlockSize = amc->GetTBSizeFromMCS (mcs, flow->GetListOfAllocatedRBs ()->size ());
          double bitsToTransmit = transportBlockSize;
          flow->UpdateAllocatedBits (bitsToTransmit);

#ifdef SCHEDULER_DEBUG
		  std::cout << "\t\t --> flow "	<< flow->GetBearer ()->GetApplication ()->GetApplicationID ()
				  << " has been scheduled: " <<
				  "\n\t\t\t nb of RBs " << flow->GetListOfAllocatedRBs ()->size () <<
				  "\n\t\t\t effectiveSinr " << effectiveSinr <<
				  "\n\t\t\t tbs " << transportBlockSize <<
				  "\n\t\t\t bitsToTransmit " << bitsToTransmit
				  << std::endl;
#endif

		  //create PDCCH messages
		  for (int rb = 0; rb < flow->GetListOfAllocatedRBs ()->size (); rb++ )
		    {
			  pdcchMsg->AddNewRecord (PdcchMapIdealControlMessage::DOWNLINK,
					  flow->GetListOfAllocatedRBs ()->at (rb),
									  flow->GetBearer ()->GetDestination (),
									  mcs);
		    }
	    }
    }

  if (pdcchMsg->GetMessage()->size () > 0)
    {
      GetMacEntity ()->GetDevice ()->GetPhy ()->SendIdealControlMessage (pdcchMsg);
    }
  delete pdcchMsg;
}
*/
void
DL_IPB_PacketScheduler::DoSchedule ()
{
#ifdef SCHEDULER_DEBUG
	std::cout << "Start DL packet scheduler for node "
			<< GetMacEntity ()->GetDevice ()->GetIDNetworkNode()<< std::endl;
#endif

  UpdateAverageTransmissionRate ();
  CheckForDLDropPackets ();
  SelectFlowsToSchedule ();
#ifdef FLOWS_TO_SCHEDULE
  //printFlowsAllInfo();
  printFlowsSimpleInfo();
#endif

  if (GetFlowsToSchedule ()->size() == 0)
	{}
  else
	{
	  RBsAllocation ();
	}

  StopSchedule ();
}

double
DL_IPB_PacketScheduler::ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel)
{
  /*
   * For the M-LWDF scheduler the metric is computed
   * as follows:
   *
   * metric = -log(dropProbability)/targetDelay *
   *  			* HOL * availableRate/averageRate
   */

  double metric;

  if ((bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_INFINITE_BUFFER)
	  ||
	  (bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_CBR))
    {
	  metric = (spectralEfficiency * 180000.)
				/
	    	    bearer->GetAverageTransmissionRate();

#ifdef SCHEDULER_DEBUG
	  std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
			 << " " << spectralEfficiency
			 << " " << bearer->GetAverageTransmissionRate ()
			 << " --> " << metric
			 << std::endl;
#endif

    }
  else
    {

     QoSForIPB *qos = (QoSForIPB*) bearer->GetQoSParameters ();

     double a = (-log10 (qos->GetDropProbability())) / qos->GetMaxDelay ();
     double HOL = bearer->GetHeadOfLinePacketDelay ();

	 metric = (a * HOL)
			 *
			 ((spectralEfficiency * 180000.)
			 /
			 bearer->GetAverageTransmissionRate ());

#ifdef SCHEDULER_DEBUG
	 std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
			 << " " << a
			 << " " << Simulator::Init()->Now()
       //<< " " << bearer->GetMacQueue()->Peek().GetTimeStamp()
       << " " << Simulator::Init()->Now() - HOL
			 << " " << HOL
			 << " " << spectralEfficiency
			 << " " << bearer->GetAverageTransmissionRate ()
			 << " --> " << metric
			 << std::endl;
#endif

    }

  return metric;
}

