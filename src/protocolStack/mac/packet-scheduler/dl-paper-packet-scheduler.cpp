#include "dl-paper-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForM_LWDF.h"
#include "../../../flows/MacQueue.h"

DL_PAPER_PacketScheduler::DL_PAPER_PacketScheduler()
{
	SetMacEntity(0);
	CreateFlowsToSchedule();
}

DL_PAPER_PacketScheduler::~DL_PAPER_PacketScheduler()
{
	Destroy();
}

void DL_PAPER_PacketScheduler::DoSchedule()
{
	#ifdef SCHEDULER_DEBUG
	std::cout << "start DL packet scheduler for node "
	          << GetMacEntity() -> GetDevice() -> GetIDNetworkNode()
	          << std::endl;
	#endif

	UpdateAverageTransmissionRate();
	CheckForDLDropPackets();
	SelectFlowsToSchedule();

	if(GetFlowsToSchedule() -> size() == 0)
	{

	}
	else
	{
		RBsAllocation();
	}

	StopSchedule();
}

double DL_PAPER_PacketScheduler::ComputeSchedulingMetric(RadioBearer* bearer, double spectralEfficiency, int subChannel)
{
	/*
	 for the PAPER scheduler the metric is computed as follows:
	 metric = α*availableRate - β*targetDelay - γ*averageRates
	 */

	double metric;
	if((bearer -> GetApplication() -> GetApplicationType() == Application::APPLICATION_TYPE_INFINITE_BUFFER)
	   ||
       (bearer -> GetApplication() -> GetApplicationType() == Application::APPLICATION_TYPE_CBR))
	{
		metric = (spectralEfficiency*180000.)
				  /
				  bearer -> GetAverageTransmissionRate();

		#ifdef SCHEDULER_DEBUG
			std::cout << "METRIC: " << bearer -> GetApplication() -> GetApplicationID()
			          << " " << spectralEfficiency
			          << " " << bearer -> GetAverageTransmissionRate()
			          << " --> " << metric
			          << std::endl;
		#endif
	}

	else
	{
		QoSForM_LWDF* qos = (QoSForM_LWDF*) bearer -> GetQoSParameters();

		double HOL = bearer -> GetHeadOfLinePacketDelay();
		
		metric = spectralEfficiency*180000. - qos->GetMaxDelay()
		       - bearer->GetAverageTransmissionRate();

		#ifdef SCHEDULER_DEBUG
		       std::cout << "METRIC: " << bearer -> GetApplication() -> GetApplicationID()
		       			 << " " << a 
		       			 << " " << Simulator::Init() -> Now()
		       			 << " " << bearer -> GetMacQueue() -> Peek().GetTimeStamp()
		       			 << " " << HOL
		       			 << " " << spectralEfficiency
		       			 << " " << bearer -> GetAverageTransmissionRate()
		       			 << " --> " << metric
		       			 << std::endl;
		#endif
	}
	return metric;
}